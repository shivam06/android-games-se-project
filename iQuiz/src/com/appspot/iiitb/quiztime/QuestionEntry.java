package com.appspot.iiitb.quiztime;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.appspot.iiitb.quiztime.core.CloudBackendFragment;
import com.appspot.iiitb.quiztime.core.CloudBackendFragment.OnListener;
import com.appspot.iiitb.quiztime.core.CloudCallbackHandler;
import com.appspot.iiitb.quiztime.core.CloudEntity;
import com.appspot.iiitb.quiztime.core.CloudQuery;
import com.appspot.iiitb.quiztime.core.CloudQuery.Order;
import com.appspot.iiitb.quiztime.core.CloudQuery.Scope;
import com.appspot.iiitb.quiztime.core.Consts;

public class QuestionEntry extends Activity implements OnListener {

	
	
	private static final String PROCESSING_FRAGMENT_TAG = "BACKEND_FRAGMENT";

	public static final String GUESTBOOK_SHARED_PREFS = "GUESTBOOK_SHARED_PREFS";
	public static final String SHOW_INTRO_PREFS_KEY = "SHOW_INTRO_PREFS_KEY";
	public static final String SCOPE_PREFS_KEY = "SCOPE_PREFS_KEY";

	private FragmentManager mFragmentManager;
	private CloudBackendFragment mProcessingFragment;

	QuestionUI qUi = new QuestionUI();
	private int quesid;

	private Button submit;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_question_entry);

		qUi.setQid((TextView) findViewById(R.id.editText1));
		qUi.setQuestion((EditText) findViewById(R.id.editText2));
		qUi.setOp1((EditText) findViewById(R.id.editText3));
		qUi.setOp2((EditText) findViewById(R.id.editText4));
		qUi.setOp3((EditText) findViewById(R.id.editText5));
		qUi.setOp4((EditText) findViewById(R.id.editText6));
		qUi.setAns((EditText) findViewById(R.id.editText7));
		submit = (Button) findViewById(R.id.button1);
		submit.setEnabled(false);
		qUi.getQid().setKeyListener(null);

		TextWatcher textwatcher = new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (qUi.getQuestion().getText().toString().equalsIgnoreCase("")
						|| qUi.getOp1().getText().toString()
								.equalsIgnoreCase("")
						|| qUi.getOp2().getText().toString()
								.equalsIgnoreCase("")
						|| qUi.getOp3().getText().toString()
								.equalsIgnoreCase("")
						|| qUi.getOp4().getText().toString()
								.equalsIgnoreCase("")
						|| qUi.getAns().getText().toString()
								.equalsIgnoreCase("")) {
					// Toast.makeText(this, "Please fill all the fields.",
					// Toast.LENGTH_LONG).show();
				} else {
					int a = Integer.parseInt(qUi.getAns().getText().toString());
					if (a >= 1 && a <= 4) {
						submit.setEnabled(true);
					} else {
						submit.setEnabled(false);
						// display some error message
					}
				}

			}
		};

		qUi.getQuestion().addTextChangedListener(textwatcher);
		qUi.getOp1().addTextChangedListener(textwatcher);
		qUi.getOp2().addTextChangedListener(textwatcher);
		qUi.getOp3().addTextChangedListener(textwatcher);
		qUi.getOp4().addTextChangedListener(textwatcher);
		qUi.getAns().addTextChangedListener(textwatcher);

		mFragmentManager = getFragmentManager();
		initiateFragments();

	}

	private void fetchQuestionID() {
		CloudCallbackHandler<List<CloudEntity>> handler = new CloudCallbackHandler<List<CloudEntity>>() {
			@Override
			public void onComplete(List<CloudEntity> result) {

				for (CloudEntity e : result) {
					Map<String, Object> prop = e.getProperties();

					Object t_qid = prop.get("qid");
					// Log.i(Consts.TAG,"Type of score fetched: "+prop.get("score").getClass().getName()
					// );

					if (t_qid.getClass().getName()
							.equalsIgnoreCase("java.lang.String")) {
						quesid = Integer.parseInt((String) t_qid);
					} else if (t_qid.getClass().getName()
							.equalsIgnoreCase("java.math.BigDecimal")) {
						
						BigDecimal b = (BigDecimal) t_qid;
						quesid = b.intValue();
					}

			//		BigDecimal q = (BigDecimal) prop.get("qid");
			//		Log.i(Consts.TAG, "type of ques id: "
			//				+ prop.get("qid").getClass().getName()
			//				+ "  value: " + prop.get("qid").toString());
					// BigDecimal q = (BigDecimal)e.get("id");

					Log.i(Consts.TAG, "Largest question id: " + quesid);
					//quesid = q.intValue();

					quesid++;
					break;
				}
				qUi.getQid().setText(quesid + "");

			}

			@Override
			public void onError(final IOException exception) {
				handleEndpointException(exception);
			}
		};

		CloudQuery cq = new CloudQuery("Question");
		cq.setSort("qid", Order.DESC);
		cq.setLimit(1);

		// mProcessingFragment.getCloudBackend().list(cq, handler)
		mProcessingFragment.getCloudBackend().listByKind("Question", "qid",
				Order.DESC, 1, Scope.PAST, handler);
	}

	private void clearFields() {
		qUi.getQid().setText("");
		qUi.getQuestion().setText("");
		qUi.getOp1().setText("");
		qUi.getOp2().setText("");
		qUi.getOp3().setText("");
		qUi.getOp4().setText("");
		qUi.getAns().setText("");
		submit.setEnabled(false);
		fetchQuestionID();

	}

	public void textWrite(View view) {

	}

	@SuppressLint("UseValueOf")
	public void saveQuestion(View view) throws IOException {

		CloudEntity ce = new CloudEntity("Question");
		// ce.setId(qUi.getQid().getText().toString());
		Integer t = new Integer(quesid);
		ce.put("qid", t);
		ce.put("question", qUi.getQuestion().getText().toString());
		ce.put("option1", qUi.getOp1().getText().toString());
		ce.put("option2", qUi.getOp2().getText().toString());
		ce.put("option3", qUi.getOp3().getText().toString());
		ce.put("option4", qUi.getOp4().getText().toString());
		ce.put("answer", qUi.getAns().getText().toString());

		// ce.put("id", "1");
		// ce.put("question", "how are you?");
		// ce.put("option1", "a");
		// ce.put("option2", "b");
		// ce.put("option3", "c");
		// ce.put("option4", "d");
		// ce.put("answer", "1");

		CloudCallbackHandler<CloudEntity> handler = new CloudCallbackHandler<CloudEntity>() {
			@Override
			public void onComplete(final CloudEntity result) {
				successUpdate();

			}

			@Override
			public void onError(final IOException exception) {
				handleEndpointException(exception);
			}
		};
		mProcessingFragment.getCloudBackend().insert(ce, handler);
		// cba.insert(ce,null);

	}

	private void successUpdate() {
		clearFields();
		Toast.makeText(this, "Question uploaded successfully!!",
				Toast.LENGTH_LONG).show();
	}

	private void handleEndpointException(IOException e) {
		Toast.makeText(this, e.toString(), Toast.LENGTH_LONG).show();
	}

	private void initiateFragments() {
		FragmentTransaction fragmentTransaction = mFragmentManager
				.beginTransaction();

		// Check to see if we have retained the fragment which handles
		// asynchronous backend calls
		mProcessingFragment = (CloudBackendFragment) mFragmentManager
				.findFragmentByTag(PROCESSING_FRAGMENT_TAG);
		// If not retained (or first time running), create a new one
		if (mProcessingFragment == null) {
			mProcessingFragment = new CloudBackendFragment();
			mProcessingFragment.setRetainInstance(true);
			fragmentTransaction.add(mProcessingFragment,
					PROCESSING_FRAGMENT_TAG);
		}

		// // Add the splash screen fragment
		// mSplashFragment = new SplashFragment();
		// fragmentTransaction.add(R.id.activity_main, mSplashFragment,
		// SPLASH_FRAGMENT_TAG);
		fragmentTransaction.commit();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.question_entry, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onCreateFinished() {
		// TODO Auto-generated method stub
		fetchQuestionID();

	}

	@Override
	public void onBroadcastMessageReceived(List<CloudEntity> message) {
		// TODO Auto-generated method stub

	}
}
