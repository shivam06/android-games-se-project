package com.appspot.iiitb.quiztime;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.appspot.iiitb.quiztime.core.CloudBackendFragment;
import com.appspot.iiitb.quiztime.core.CloudBackendFragment.OnListener;
import com.appspot.iiitb.quiztime.core.CloudCallbackHandler;
import com.appspot.iiitb.quiztime.core.CloudEntity;
import com.appspot.iiitb.quiztime.core.CloudQuery.Order;
import com.appspot.iiitb.quiztime.core.CloudQuery.Scope;
import com.appspot.iiitb.quiztime.core.Filter;

public class LeaderBoard extends Activity implements OnListener{

	private static final String PROCESSING_FRAGMENT_TAG = "BACKEND_FRAGMENT";
	private static final int INTRO_ACTIVITY_REQUEST_CODE = 1;


	private FragmentManager mFragmentManager;
	private CloudBackendFragment mProcessingFragment;

	public static final String GUESTBOOK_SHARED_PREFS = "GUESTBOOK_SHARED_PREFS";
	public static final String SHOW_INTRO_PREFS_KEY = "SHOW_INTRO_PREFS_KEY";
	public static final String SCOPE_PREFS_KEY = "SCOPE_PREFS_KEY";

	ListView listV;
	ArrayList<String> list=new ArrayList<String>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_leader_board);
		mFragmentManager = getFragmentManager();
		initiateFragments();
		
		
	}

	private void initiateFragments() {
		FragmentTransaction fragmentTransaction = mFragmentManager
				.beginTransaction();

		// Check to see if we have retained the fragment which handles
		// asynchronous backend calls
		mProcessingFragment = (CloudBackendFragment) mFragmentManager
				.findFragmentByTag(PROCESSING_FRAGMENT_TAG);
		// If not retained (or first time running), create a new one
		if (mProcessingFragment == null) {

			mProcessingFragment = new CloudBackendFragment();
			mProcessingFragment.setRetainInstance(true);
			fragmentTransaction.add(mProcessingFragment,
					PROCESSING_FRAGMENT_TAG);
		}

		// // Add the splash screen fragment
		// mSplashFragment = new SplashFragment();
		// fragmentTransaction.add(R.id.activity_main, mSplashFragment,
		// SPLASH_FRAGMENT_TAG);
		fragmentTransaction.commit();

	}
	
	private void setLeaderBoard(){
		listV = (ListView) findViewById(R.id.listView1);
		listV.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list));   
	
	}
	
	private void fetchLeaderBoard(){
		
	
		
		CloudCallbackHandler<List<CloudEntity>> handler = new CloudCallbackHandler<List<CloudEntity>>() {

			@Override
			public void onComplete(List<CloudEntity> results) {
				
				// TODO Auto-generated method stub
				for(CloudEntity e:results){
					Map<String,Object> prop = e.getProperties();
					list.add(e.getCreatedBy() + "     score: "+prop.get("score"));
				}
				setLeaderBoard();
			}
		};
		mProcessingFragment.getCloudBackend().listByProperty("Registered_Users", "score", Filter.Op.GE, 0, Order.DESC, 20, Scope.PAST, handler);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.leader_board, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onCreateFinished() {
		// TODO Auto-generated method stub
		fetchLeaderBoard();
		
	}

	@Override
	public void onBroadcastMessageReceived(List<CloudEntity> message) {
		// TODO Auto-generated method stub
		
	}
}
