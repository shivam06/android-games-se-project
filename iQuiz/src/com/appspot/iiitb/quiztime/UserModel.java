package com.appspot.iiitb.quiztime;

public class UserModel {
	
	String email;
	int score;
	int currentqid;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public int getCurrentqid() {
		return currentqid;
	}
	public void setCurrentqid(int currentqid) {
		this.currentqid = currentqid;
	}
	
}
