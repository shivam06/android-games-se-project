package com.appspot.iiitb.quiztime;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.appspot.iiitb.quiztime.core.CloudBackendFragment;
import com.appspot.iiitb.quiztime.core.CloudBackendFragment.OnListener;
import com.appspot.iiitb.quiztime.core.CloudCallbackHandler;
import com.appspot.iiitb.quiztime.core.CloudEntity;
import com.appspot.iiitb.quiztime.core.CloudQuery;
import com.appspot.iiitb.quiztime.core.CloudQuery.Order;
import com.appspot.iiitb.quiztime.core.CloudQuery.Scope;
import com.appspot.iiitb.quiztime.core.Consts;
import com.appspot.iiitb.quiztime.core.Filter;

public class QuizActivity extends Activity implements OnListener {

	private TextView qid;
	private TextView question;
	private TextView option1;
	private TextView option2;
	private TextView option3;
	private TextView option4;
	private String answer;
	private TextView message;
	private TextView disp_score;
	private TextView answerfield;
	private Button check;
	private RadioGroup  rg;
	
	private List<CloudEntity> questions;
	private int currentqid = 1;

	private static final String PROCESSING_FRAGMENT_TAG = "BACKEND_FRAGMENT";

	public static final String GUESTBOOK_SHARED_PREFS = "GUESTBOOK_SHARED_PREFS";
	public static final String SHOW_INTRO_PREFS_KEY = "SHOW_INTRO_PREFS_KEY";
	public static final String SCOPE_PREFS_KEY = "SCOPE_PREFS_KEY";

	private FragmentManager mFragmentManager;
	private CloudBackendFragment mProcessingFragment = null;

	private String email; // recd. from prev. intent
	private Integer score;
	private BigDecimal b_score;
	private Integer userqid;
	

	private String myanswer; // record from this question
	private int myscore; // maintain throughout

	CloudEntity user = null;

	/**
	 * A list of posts to be displayed
	 */

	/**
	 * Override Activity lifecycle method.
	 */

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Get the message from the intent
		Intent intent = getIntent();
		email = intent.getStringExtra(com.appspot.iiitb.quiztime.Menu.EMAIL);
		score = Integer.valueOf(intent.getStringExtra(com.appspot.iiitb.quiztime.Menu.SCORE));
		userqid = Integer.valueOf(intent
				.getStringExtra(com.appspot.iiitb.quiztime.Menu.CURRENTQID));

		setContentView(R.layout.activity_quiz);

		qid = (TextView) findViewById(R.id.textView1);
		question = (TextView) findViewById(R.id.textView2);
//		option1 = (TextView) findViewById(R.id.textView4);
//		option2 = (TextView) findViewById(R.id.textView6);
//		option3 = (TextView) findViewById(R.id.textView8);
//		option4 = (TextView) findViewById(R.id.textView10);
		
		rg = (RadioGroup) findViewById(R.id.radioGroup1);
		option1 = (RadioButton) findViewById(R.id.roption1);
		option2 = (RadioButton) findViewById(R.id.roption2);
		option3 = (RadioButton) findViewById(R.id.roption3);
		option4 = (RadioButton) findViewById(R.id.roption4);
		
		message = (TextView) findViewById(R.id.textView12);
		disp_score = (TextView) findViewById(R.id.textView14);
		answerfield = (TextView) findViewById(R.id.editText1);
		check = (Button) findViewById(R.id.button2);
		check.setEnabled(false);
		currentqid = userqid.intValue();
		myscore = score.intValue();
		radio(false);
		
		mFragmentManager = getFragmentManager();

		initiateFragments();
	}

	private void initializeUserEntity() {
		CloudCallbackHandler<List<CloudEntity>> handler = new CloudCallbackHandler<List<CloudEntity>>() {
			@Override
			public void onComplete(List<CloudEntity> results) {

				if (results != null) {
					user = results.get(0);
				}
				try {
					listQuestion();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			@Override
			public void onError(IOException exception) {

				handleEndpointException(exception);
			}
		};

		mProcessingFragment.getCloudBackend().listByProperty(
				"Registered_Users", "_createdBy", Filter.Op.EQ, email,
				Order.ASC, 1, Scope.PAST, handler);
	}

	/**
	 * Override Activity lifecycle method.
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_main, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public void onCreateFinished() {
		// listPosts();
		initializeUserEntity();
		
	}

	public void onRadioButtonClicked(View view) {
		check.setEnabled(true);
	}

	private void findRadioSelected() {
		RadioGroup rg = (RadioGroup) findViewById(R.id.radioGroup1);

		int rbId = rg.getCheckedRadioButtonId();

		switch (rbId) {
		case R.id.roption1:
			myanswer = "1";
			break;
		case R.id.roption2:
			myanswer = "2";
			break;
		case R.id.roption3:
			myanswer = "3";
			break;
		case R.id.roption4:
			myanswer = "4";
			break;
		}
	}

	public void submit(View view) {

		findRadioSelected();

		CloudCallbackHandler<CloudEntity> handler = new CloudCallbackHandler<CloudEntity>() {
			@Override
			public void onComplete(CloudEntity results) {
				user = results;
				try {
					listQuestion();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			@Override
			public void onError(IOException exception) {

				handleEndpointException(exception);
			}
		};

		// here menu item from view was fetched
		
		if (myanswer.equalsIgnoreCase(answer)) {
			message.setTextColor(Color.rgb(0, 200, 0));
			message.setText("Answer is correct!!!");

			myscore++;
			currentqid = Integer.parseInt(qid.getText().toString()) + 1;

			score = Integer.valueOf(myscore);
			userqid = Integer.valueOf(currentqid);

			user.put("score", score);
			user.put("atqid", userqid);
			clearView();
			mProcessingFragment.getCloudBackend().update(user, handler);
		
		} else {
			message.setTextColor(Color.rgb(200, 0, 0));
			message.setText("Answer is wrong!!! Try again..");
			
		}
	}

	private void listQuestion() throws IOException {

		// CloudBackendAsync cba = new
		// CloudBackendAsync(getApplicationContext());
		// CloudQuery cq = new CloudQuery("Question");
		// cq.setFilter(Filter.eq("id", "1"));
		// cq.setLimit(1);
		// ques = cba.list(cq).get(0);

		// Filter F = new Filter();
		CloudCallbackHandler<List<CloudEntity>> handler = new CloudCallbackHandler<List<CloudEntity>>() {
			@Override
			public void onComplete(List<CloudEntity> results) {
				questions = results;
				updateQuizView();
			}

			@Override
			public void onError(IOException exception) {

				handleEndpointException(exception);
			}
		};
		//
		// cba.listByKind("Question", "id", Order.ASC, 100, Scope.PAST,
		// handler);

		Log.i(Consts.TAG, "fetching question of id: "+currentqid);
		Integer t=Integer.valueOf(currentqid);
		CloudQuery cq = new CloudQuery("Question");
		cq.setFilter(Filter.eq("qid", t));
		cq.setLimit(1);
		mProcessingFragment.getCloudBackend().list(cq, handler);
		
//		mProcessingFragment.getCloudBackend().listByProperty("Question", "id",
//				Filter.Op.EQ, Integer.valueOf(currentqid), Order.ASC, 1, Scope.PAST,
//				handler);
		
		
		
		// mProcessingFragment.getCloudBackend().listByKind("Question", "id",
		// Order.ASC, 100, Scope.PAST, handler);
		// if(mProcessingFragment.getCloudBackend()==null){
		// Toast.makeText(this, "getcloudbackend is null",
		// Toast.LENGTH_LONG).show();
		// }
		// cba.listByProperty("Question", "id", F.op, 1, Order.DESC, 1,
		// Scope.PAST, handler);
	}

	private void initiateFragments() {
		FragmentTransaction fragmentTransaction = mFragmentManager
				.beginTransaction();

		// Check to see if we have retained the fragment which handles
		// asynchronous backend calls
		mProcessingFragment = (CloudBackendFragment) mFragmentManager
				.findFragmentByTag(PROCESSING_FRAGMENT_TAG);
		// If not retained (or first time running), create a new one
		if (mProcessingFragment == null) {
			mProcessingFragment = new CloudBackendFragment();
			mProcessingFragment.setRetainInstance(true);
			fragmentTransaction.add(mProcessingFragment,
					PROCESSING_FRAGMENT_TAG);
		}

		// // Add the splash screen fragment
		// mSplashFragment = new SplashFragment();
		// fragmentTransaction.add(R.id.activity_main, mSplashFragment,
		// SPLASH_FRAGMENT_TAG);
		fragmentTransaction.commit();

	}

	public void nextQuestion(View view) {

		try {
			listQuestion();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// updateQuizView();
	}

	
	
	private void radio(boolean value){
		RadioButton rb1 = (RadioButton)findViewById(R.id.roption1);
		RadioButton rb2 = (RadioButton)findViewById(R.id.roption2);
		RadioButton rb3 = (RadioButton)findViewById(R.id.roption3);
		RadioButton rb4 = (RadioButton)findViewById(R.id.roption4);
		rb1.setEnabled(value);
		rb2.setEnabled(value);
		rb3.setEnabled(value);
		rb4.setEnabled(value);
	}
	private void clearView() {
		qid.setText(currentqid + "");
		disp_score.setText(score.intValue()+"");
		question.setText("loading...");
		option1.setText("...");
		option2.setText("...");
		option3.setText("...");
		option4.setText("...");
		//answerfield.setText("");
		rg.clearCheck();
		rg.setEnabled(false);
		rg.setClickable(false);
		rg.setFocusable(false);
		radio(false);
		check.setEnabled(false);
		
		disp_score.setText(score.intValue()+"");
	}

	private void updateQuizView() {
		message.setText("");
		
		for (CloudEntity e : questions) {
			
			
			Map<String, Object> p = e.getProperties();
//			BigDecimal b = (BigDecimal) p.get("qid");
			Object b = p.get("qid");
			boolean flag;
			if(b.getClass().getName()
						.equalsIgnoreCase("java.lang.String")){
				if(Integer.parseInt(b.toString()) == currentqid) flag = true;
				else flag = false;
			}else{
				BigDecimal t = (BigDecimal) b;
				if(t.intValue()==currentqid) flag = true;
				else flag =false;
			}
			if(b!=null && flag){
				radio(true);
				rg.setEnabled(true);
				rg.setClickable(true);
				rg.setFocusable(true);
				
				//if (Integer.parseInt(e.get("id").toString()) == currentqid) {
				
				Map<String, Object> prop = e.getProperties();
				
				Object t_qid = prop.get("qid");
				// Log.i(Consts.TAG,"Type of score fetched: "+prop.get("score").getClass().getName()
				// );

				if (t_qid.getClass().getName()
						.equalsIgnoreCase("java.lang.String")) {
					qid.setText(t_qid.toString());
				} else if (t_qid.getClass().getName()
						.equalsIgnoreCase("java.math.BigDecimal")) {
					
					BigDecimal bt = (BigDecimal) t_qid;
					qid.setText(bt.intValue()+"");
				}
				
//				BigDecimal bt = (BigDecimal)prop.get("qid");
//				qid.setText(bt.toString());
				question.setText((String) prop.get("question"));
				option1.setText((String) prop.get("option1"));
				option2.setText((String) prop.get("option2"));
				option3.setText((String) prop.get("option3"));
				option4.setText((String) prop.get("option4"));
				disp_score.setText(score.intValue()+"");
				answer = (String) prop.get("answer");
				break;
			}else{
				Log.i(Consts.TAG, "Fetched question is null");
			}
			
			
			if(questions == null){
				radio(false);
				// question.setText("Quiz FINISHED!!!");
				message.setText("Quiz FINISHED!!!");
				Toast.makeText(this, "All questions finished!!!",
						Toast.LENGTH_LONG).show();
			}
		}
	}

	private void handleEndpointException(IOException e) {
		Toast.makeText(this, e.toString(), Toast.LENGTH_LONG).show();
		// mSendBtn.setEnabled(true);
	}
	
	
	@Override
	public void onBackPressed() {

		String t1 = userqid.intValue()+"";
		String t2 = myscore+"";
	    if (t1 != null && t2!=null){
	    	Intent intent = new Intent();
			
			intent.putExtra(com.appspot.iiitb.quiztime.Menu.CURRENTQID, t1);
			intent.putExtra(com.appspot.iiitb.quiztime.Menu.SCORE, t2);
			setResult(RESULT_OK,intent);  
			Log.i(Consts.TAG, "Pausing now.... currentqid: "+t1+ " and score: "+t2);
	        finish();
	    } else {
	        Intent returnIntent = new Intent();
	        setResult(RESULT_CANCELED, returnIntent);        
	        finish();
	    }
	    super.onBackPressed();
	}

	@Override
	public void onBroadcastMessageReceived(List<CloudEntity> message) {
		// TODO Auto-generated method stub

	}

}
