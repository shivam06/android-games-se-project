package com.appspot.iiitb.quiztime;

import android.widget.TextView;

public class QuestionUI {

	private TextView qid,question,op1,op2,op3,op4,ans;

	
	public TextView getQid() {
		return qid;
	}
	public void setQid(TextView qid) {
		this.qid = qid;
	}
	public TextView getQuestion() {
		return question;
	}
	public void setQuestion(TextView question) {
		this.question = question;
	}
	public TextView getOp1() {
		return op1;
	}
	public void setOp1(TextView op1) {
		this.op1 = op1;
	}
	public TextView getOp2() {
		return op2;
	}
	public void setOp2(TextView op2) {
		this.op2 = op2;
	}
	public TextView getOp3() {
		return op3;
	}
	public void setOp3(TextView op3) {
		this.op3 = op3;
	}
	public TextView getOp4() {
		return op4;
	}
	public void setOp4(TextView op4) {
		this.op4 = op4;
	}
	public TextView getAns() {
		return ans;
	}
	public void setAns(TextView ans) {
		this.ans = ans;
	}

	
}
