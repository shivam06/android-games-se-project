package com.appspot.iiitb.quiztime;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.appspot.iiitb.quiztime.core.CloudBackendFragment;
import com.appspot.iiitb.quiztime.core.CloudBackendFragment.OnListener;
import com.appspot.iiitb.quiztime.core.CloudCallbackHandler;
import com.appspot.iiitb.quiztime.core.CloudEntity;
import com.appspot.iiitb.quiztime.core.CloudQuery.Order;
import com.appspot.iiitb.quiztime.core.CloudQuery.Scope;
import com.appspot.iiitb.quiztime.core.Consts;
import com.appspot.iiitb.quiztime.core.Filter;
import com.appspot.iiitb.quiztime.sample.guestbook.GuestbookActivity;
import com.appspot.iiitb.quiztime.sample.guestbook.IntroductionActivity;
import com.appspot.iiitb.quiztime.util.SystemUiHider;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 * 
 * @see SystemUiHider
 */
public class Menu extends Activity implements OnListener {

	private static final String BROADCAST_PROP_DURATION = "duration";
	private static final String BROADCAST_PROP_MESSAGE = "message";

	private static final String QUESTION_UPLOAD_ID = "shivamlearning@gmail.com";

	private static final String PROCESSING_FRAGMENT_TAG = "BACKEND_FRAGMENT";
	private static final int INTRO_ACTIVITY_REQUEST_CODE = 1;
	private boolean showIntro = true;

	private FragmentManager mFragmentManager;
	private CloudBackendFragment mProcessingFragment;

	public static final String GUESTBOOK_SHARED_PREFS = "GUESTBOOK_SHARED_PREFS";
	public static final String SHOW_INTRO_PREFS_KEY = "SHOW_INTRO_PREFS_KEY";
	public static final String SCOPE_PREFS_KEY = "SCOPE_PREFS_KEY";
	public final static String EMAIL = "com.appspot.iiitb.quiztime.EMAIL";
	public final static String CURRENTQID = "com.appspot.iiitb.quiztime.CURRENTQID";
	public final static String SCORE = "com.appspot.iiitb.quiztime.SCORE";

	private TextView t_score;
	private String email;
	private String currentqid;
	private String score;
	private int LIMIT = 100;
	private int BATCH = 1;
	private int count = 0;
	private BufferedReader buffer = null;
	public void onUploadQuestions(View view) {
		count = 0;
		try {
			BufferedReader buf = new BufferedReader(new FileReader(
					Environment.getExternalStorageDirectory()
							+ "/Questions.txt"));
			buffer = buf;
			callAgain(buffer);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			Log.i(Consts.TAG, e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			// TODO: handle exception
			Log.i(Consts.TAG, e.getMessage());
			e.printStackTrace();
		}
	}

	public void callAgain(BufferedReader buf) {

		CloudCallbackHandler<List<CloudEntity>> handler = new CloudCallbackHandler<List<CloudEntity>>() {

			@Override
			public void onComplete(List<CloudEntity> results) {
				// TODO Auto-generated method stub
				count =0;
				Log.i(Consts.TAG, "BATCH " + BATCH + " successfully updated!!");
				BATCH++;
				callAgain(buffer);
				

			}

		};

		List<CloudEntity> l = new ArrayList<CloudEntity>();
		try {

			String currentline = null;
			// FileReader fr = new
			// FileReader(Environment.getExternalStorageDirectory()
			// + "/Questions.txt");

			while ((currentline = buf.readLine()) != null && count<LIMIT) {
				CloudEntity c = new CloudEntity("Question");

				String[] parts = currentline.split(",");

				Integer q = Integer.valueOf(parts[0]);

				c.put("qid", q);
				c.put("question", parts[1]);
				c.put("option1", parts[2]);
				c.put("option2", parts[3]);
				c.put("option3", parts[4]);
				c.put("option4", parts[5]);
				c.put("answer", parts[6]);
				l.add(c);
				count++;

//				if (count < LIMIT) {
//
//				} else {
//					
//					List<CloudEntity> temp = new ArrayList<CloudEntity>();
//					for(CloudEntity t:l){
//						Map<String,Object> prop = t.getProperties();
//						CloudEntity ce = new CloudEntity("Question");
//						ce.put("qid", prop.get("id"));
//						ce.put("question", prop.get("question"));
//						ce.put("option1", prop.get("option1"));
//						ce.put("option2", prop.get("option2"));
//						ce.put("option3", prop.get("option3"));
//						ce.put("option4", prop.get("option4"));
//						ce.put("answer", prop.get("answer"));
//						temp.add(ce);
//					}
//					l.clear();
//					count = 0;
//					mProcessingFragment.getCloudBackend().insertAll(l, handler);
//					BATCH++;
//					
//				
//				}

			}
			mProcessingFragment.getCloudBackend().insertAll(l, handler);


			// Log.i(Consts.TAG, "REad value: " + currentline);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			Log.i(Consts.TAG, e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			// TODO: handle exception
			Log.i(Consts.TAG, e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			// TODO: handle exception
			Log.i(Consts.TAG, e.getMessage());
			e.printStackTrace();
		}

	}

	/**
	 * Override Activity lifecycle method.
	 */
	@Override
	public boolean onPrepareOptionsMenu(android.view.Menu menu) {
		MenuItem loginItem = menu.findItem(R.id.switch_account);
		loginItem.setVisible(Consts.IS_AUTH_ENABLED);
		return true;
	}

	@Override
	public boolean onCreateOptionsMenu(android.view.Menu menu) {
		// TODO Auto-generated method stub
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_main, menu);
		return super.onCreateOptionsMenu(menu);
	}

	/**
	 * Override Activity lifecycle method.
	 * <p>
	 * To add more option menu items in your client, add the item to
	 * menu/activity_main.xml, and provide additional case statements in this
	 * method.
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.switch_account:
			mProcessingFragment.signInAndSubscribe(true);
			Button b = (Button) findViewById(R.id.button6);
			if (mProcessingFragment.getAccountName().equalsIgnoreCase(
					QUESTION_UPLOAD_ID)) {

				b.setVisibility(View.VISIBLE);
			} else {
				b.setVisibility(View.INVISIBLE);
			}
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		this.showIntro = false;
	}

	public void startQuiz(View view) {
		Intent intent = new Intent(this, QuizActivity.class);
		intent.putExtra(EMAIL, email);
		intent.putExtra(CURRENTQID, currentqid);
		intent.putExtra(SCORE, score);
		intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
		intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
		startActivityForResult(intent, 2);
	}

	public void startDiscussion(View view) {
		Intent intent = new Intent(this, GuestbookActivity.class);
		intent.putExtra(EMAIL, email);
		intent.putExtra(CURRENTQID, currentqid);
		intent.putExtra(SCORE, score);
		intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
		intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
		startActivity(intent);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		Log.i(Consts.TAG, "after returning. request code: " + requestCode
				+ "resultcode: " + resultCode + " RESULTOF " + RESULT_OK);

		// check if the request code is same as what is passed here it is 2
		if (requestCode == 2) {
			// fetch the message String
			if (resultCode == RESULT_OK) {

				score = data.getExtras().getString(
						com.appspot.iiitb.quiztime.Menu.SCORE);
				if (score == null) {
					Log.i(Consts.TAG,
							"returned value of score from activity is null");
				} else {
					Log.i(Consts.TAG,
							"returned value of score from activity is not null. score: "
									+ score);
				}
				currentqid = data.getExtras().getString(
						com.appspot.iiitb.quiztime.Menu.CURRENTQID);
				t_score.setText(score);
			}

		}

		if (requestCode == INTRO_ACTIVITY_REQUEST_CODE) {
			initiateFragments();
		}

	}

	public void questionEntry(View view) {
		Intent intent = new Intent(this, QuestionEntry.class);
		// intent.putExtra("fragment", (Object)mProcessingFragment);
		startActivity(intent);
	}

	public void leaderBoard(View view) {

		Intent intent = new Intent(this, LeaderBoard.class);
		startActivity(intent);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_menu);
		t_score = (TextView) findViewById(R.id.textView2);
		mFragmentManager = getFragmentManager();
		checkForPreferences();
	}

	private void checkForPreferences() {
		SharedPreferences settings = getSharedPreferences(
				GUESTBOOK_SHARED_PREFS, Context.MODE_PRIVATE);
		boolean showIntro = true;
		if (settings != null) {
			showIntro = settings.getBoolean(SHOW_INTRO_PREFS_KEY, true)
					&& this.showIntro;
		}
		if (showIntro) {
			Intent intent = new Intent(this, IntroductionActivity.class);
			startActivityForResult(intent, INTRO_ACTIVITY_REQUEST_CODE);
		}
		initiateFragments();

	}

	private void initiateFragments() {
		FragmentTransaction fragmentTransaction = mFragmentManager
				.beginTransaction();

		// Check to see if we have retained the fragment which handles
		// asynchronous backend calls
		mProcessingFragment = (CloudBackendFragment) mFragmentManager
				.findFragmentByTag(PROCESSING_FRAGMENT_TAG);
		// If not retained (or first time running), create a new one
		if (mProcessingFragment == null) {

			mProcessingFragment = new CloudBackendFragment();
			mProcessingFragment.setRetainInstance(true);
			fragmentTransaction.add(mProcessingFragment,
					PROCESSING_FRAGMENT_TAG);
		}

		// // Add the splash screen fragment
		// mSplashFragment = new SplashFragment();
		// fragmentTransaction.add(R.id.activity_main, mSplashFragment,
		// SPLASH_FRAGMENT_TAG);
		fragmentTransaction.commit();

	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);

	}

	private void checkAndRegister() {
		email = mProcessingFragment.getAccountName().toString();
		CloudCallbackHandler<List<CloudEntity>> handler = new CloudCallbackHandler<List<CloudEntity>>() {

			@Override
			public void onComplete(List<CloudEntity> results) {
				// TODO Auto-generated method stub
				fetchRegInfo(results);
			}

		};

		CloudEntity temp = new CloudEntity("Registered_Users");
		temp.setId(email);

		// mProcessingFragment.getCloudBackend().listByKind("Registered_Users",
		// "ID", Order.ASC, 1000, Scope.PAST, handler);
		mProcessingFragment.getCloudBackend().listByProperty(
				"Registered_Users", "_createdBy", Filter.Op.EQ, email,
				Order.ASC, 10, Scope.PAST, handler);
	}

	private void registerSuccessfull() {
		Log.i(Consts.TAG, "User registered successfully!!");
		Toast.makeText(getApplicationContext(), "New user registered!!",
				Toast.LENGTH_LONG).show();
	}

	private void fetchRegInfo(List<CloudEntity> result) {

		CloudCallbackHandler<CloudEntity> handler3 = new CloudCallbackHandler<CloudEntity>() {

			@Override
			public void onComplete(CloudEntity results) {
				// TODO Auto-generated method stub
				registerSuccessfull();
			}
		};

		boolean registered = false;
		for (CloudEntity e : result) {

			if (e.getCreatedBy().equalsIgnoreCase(email)) {
				// Already registered

				Map<String, Object> prop = e.getProperties();

				Object t_score = prop.get("score");
				Object t_atqid = prop.get("atqid");
				// Log.i(Consts.TAG,"Type of score fetched: "+prop.get("score").getClass().getName()
				// );

				if (t_score.getClass().getName()
						.equalsIgnoreCase("java.lang.String")) {
					score = (String) t_score;
					currentqid = (String) t_atqid;
				} else if (t_score.getClass().getName()
						.equalsIgnoreCase("java.math.BigDecimal")) {
					BigDecimal b = (BigDecimal) t_score;
					score = b.intValue() + "";
					BigDecimal bq = (BigDecimal) prop.get("atqid");
					currentqid = bq.intValue() + "";
				}

				// score = (String) prop.get("score");
				// currentqid = (String) prop.get("atqid");

				Log.i(Consts.TAG, "score: " + score + "current qid: "
						+ currentqid);
				// Toast.makeText(this, "score: "+score +
				// "current qid: "+currentqid, Toast.LENGTH_LONG).show();
				registered = true;
				break;
			}
		}
		if (registered == false) {
			// user not registered. Register it.
			CloudEntity user = new CloudEntity("Registered_Users");
			user.setId(email);
			user.put("score", Integer.valueOf(0));
			user.put("atqid", Integer.valueOf(1));
			currentqid = "1";
			score = "0";
			mProcessingFragment.getCloudBackend().insert(user, handler3);
		} else {
			// Already registered.
			Log.i(Consts.TAG, "User already registered!!");
			Toast.makeText(getApplicationContext(), "Already registered!!",
					Toast.LENGTH_LONG).show();
		}
		TextView s = (TextView) findViewById(R.id.textView2);
		s.setText(score);
	}

	@Override
	public void onCreateFinished() {
		// TODO Auto-generated method stub

		checkAndRegister();
		// try {
		// Thread.currentThread().join();
		// } catch (InterruptedException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		Toast.makeText(this, mProcessingFragment.getAccountName(),
				Toast.LENGTH_LONG).show();
		Button b = (Button) findViewById(R.id.button6);
		if (mProcessingFragment.getAccountName().equalsIgnoreCase(
				QUESTION_UPLOAD_ID)) {

			b.setVisibility(View.VISIBLE);
		} else {
			b.setVisibility(View.INVISIBLE);
		}
		Log.i(Consts.TAG, "score is: " + score);

	}

	@Override
	public void onBroadcastMessageReceived(List<CloudEntity> l) {
		// TODO Auto-generated method stub
		for (CloudEntity e : l) {
			String message = (String) e.get(BROADCAST_PROP_MESSAGE);
			int duration = Integer.parseInt((String) e
					.get(BROADCAST_PROP_DURATION));
			Toast.makeText(this, message, duration).show();
			Log.i(Consts.TAG, "A message was recieved with content: " + message);
		}

	}

}
